from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    p = (52.2053, 0.1218)

    stations = build_station_list()

    sorted_distance = stations_by_distance(stations, p)

    closest = sorted_distance[:10]
    farthest = sorted_distance[-10:]

    closest_tuple_list = []
    farthest_tuple_list = []

    for i in range (0,10):
        closest_tuple_list.append((closest[i][0].name, closest[i][0].town, closest[i][1]))
        farthest_tuple_list.append((farthest[i][0].name, farthest[i][0].town, farthest[i][1]))

    print("Ten closest stations to Cambridge city centre: ")
    print(closest_tuple_list)
    print(" ")
    print("Ten farthest stations to Cambridge city centre: ")
    print(farthest_tuple_list)

if __name__ == "__main__":
    run()