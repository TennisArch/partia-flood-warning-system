from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():    
    stations = build_station_list()
    centre = (52.2053, 0.1218)
    r = 10

    valid_stations = stations_within_radius(stations, centre, r)

    name_list = []
    for station in valid_stations:
        name_list.append(station.name)

    print("Stations within 10km of Cambridge City Centre:")
    print(sorted(name_list))

if __name__ == "__main__":
    run()

