from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list

def run():  
    stations = build_station_list()
    rivers = rivers_with_stations(stations)
    print("No. of rivers with at least 1 station: " + str(len(rivers)))

    print("First ten rivers with stations alphabetically:")
    rivers_list = list(rivers)
    rivers_list.sort()
    print(rivers_list[:10])

    print("")


    dict = stations_by_river(stations)
    print("Stations on the River Aire:")
    print(dict['River Aire'])
    print("Stations on the River Cam:")
    print(dict['River Cam'])
    print("Stations on the River Thames:")
    print(dict['River Thames'])

if __name__ == "__main__":
    run()