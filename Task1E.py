from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():   
    stations = build_station_list()
    top_N_rivers = rivers_by_station_number(stations, 9)

    print("Top 9 rivers with the most stations on them (counting tied positions as one when necessary):")
    print(top_N_rivers)

if __name__ == "__main__":
    run()