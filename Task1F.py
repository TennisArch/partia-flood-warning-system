from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():  
    stations = build_station_list()
    output = inconsistent_typical_range_stations(stations)
    print("The following are stations without data or have a low value above their max value")
    print(output)

if __name__ == "__main__":
    run()
