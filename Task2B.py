from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold

def run():
    """Uses the function in flood.py to print a list of tuples"""
    
    stations = build_station_list()

    update_water_levels(stations)

    stations_over = stations_level_over_threshold(stations, 0.8)

    return print(stations_over)
        
if __name__ == '__main__':
    run()