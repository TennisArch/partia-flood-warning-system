from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    stations = build_station_list()

    update_water_levels(stations)

    highest_relative_stations = stations_highest_rel_level(stations, 10)

    return print(highest_relative_stations)

if __name__ == "__main__":
    run()