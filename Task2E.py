from floodsystem.stationdata import build_station_list, update_water_levels
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    stations = build_station_list()
    update_water_levels(stations)

    dt = 10

    highest_rel_level = stations_highest_rel_level(stations, 5)
    print(highest_rel_level)

    for station in stations:
        for item in highest_rel_level:
            if station.name in item[0]:
                dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
                plot_water_levels(station, dates, levels)


if __name__ == "__main__":
    run()