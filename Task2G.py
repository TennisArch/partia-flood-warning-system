from floodsystem import risk_assessment
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    low, moderate, high, severe = risk_assessment.assess_risk(stations)

    severe_risk_towns = risk_assessment.towns_at_risk(severe)
    print("towns with severe risk of flooding: ")
    print(severe_risk_towns)



if __name__ == "__main__":
    run()