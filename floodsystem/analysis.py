import matplotlib
import numpy as np

def polyfit(dates, levels, p, test_mode = False):
    '''This function takes 4 arguments, a list of dates, a list of levels, p (the order of the polynomial to be
    generated), and test mode true or false. Next, the function converts the dates to floats, and then uses a change of
    variable to improve to polynomial fit. In test mode, this is bypassed. A polynomial fit is the generated.'''


    if test_mode == False:
        float_dates = matplotlib.dates.date2num(dates)
        poly_float_dates = []
        for date in float_dates:
            poly_float_dates.append(date - float_dates[0])
    else:
        float_dates = dates
        poly_float_dates = dates

    date_min = float_dates[0]
    date_max = float_dates[(len(float_dates)-1)]

    p_coeff = np.polyfit(poly_float_dates, levels, p)
    poly = np.poly1d(p_coeff)

    return poly_float_dates, poly