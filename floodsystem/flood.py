from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels

def stations_level_over_threshold(stations, tol):
    """This function uses the relative water level function in the monitoring station class
    and builds a list of stations that have a relative level value greater than the tolerance"""


    stations_over_level = []
    for station in stations:
        
        if MonitoringStation.relative_water_level(station) is not None:
            
            if MonitoringStation.relative_water_level(station) > tol:
                stations_over_level.append((station.name, MonitoringStation.relative_water_level(station)))
                
    stations_over_level.sort(key=lambda x: x[1], reverse=True)
    return stations_over_level

def stations_highest_rel_level(stations, N):
    """This function uses the relative water level function in the monitoring station class
    and builds a list of stations that have a relative level value greater than 0. Then it takes 
    the first N in the sorted list and appends them to a new one that is returned"""
    
    stations_over_level = []
    highest_rel =[]
    for station in stations:
        
        if MonitoringStation.relative_water_level(station) is not None:
            
            if MonitoringStation.relative_water_level(station) > 0:
                stations_over_level.append((station.name, MonitoringStation.relative_water_level(station)))
                
    stations_over_level.sort(key=lambda x: x[1], reverse=True)

    if N <= len(stations_over_level):
        for i in range(N):
            highest_rel.append(stations_over_level[i])
    else:
        return "Please enter a valid N"
    return highest_rel

