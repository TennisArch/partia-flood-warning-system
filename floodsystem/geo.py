# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from haversine import haversine
from floodsystem.utils import sorted_by_key  # noqa


def stations_by_distance(stations, p):
    """This function takes 2 arguments. The first is a list of all the stations taken from the flood station database.
    The second argument is a tuple with two elements, which is the coordinates of a point.
    The function calculates the distance between each flood station and the point passed in. The function compiles a
    list of 2 element tuples, with the station being the first element, and the calculated distance the second element.
    This list is then sorted by distance and returned"""

    distance_tuples_list = []
    for station in stations:
        distance = haversine(station.coord, p)
        distance_tuples_list.append((station, distance))

    return_list = sorted_by_key(distance_tuples_list, 1)

    return return_list


def stations_within_radius(stations, centre, r):
    """Function expects 3 arguments: a list of all stations, a centre coordinate tuple, and a radius. It calculates
    the distance from the centre to the station, and if that distance is less than of equal to the radius, adds the
    station to a list. The list is then returned."""

    in_radius = []
    for station in stations:
        distance = haversine(station.coord, centre)
        if distance <= r:
            in_radius.append(station)

    return in_radius


def rivers_with_stations(stations):
    """function expects 1 argument: a list of station objects. The function iterates through the list, and adds
    each river to a set called rivers. The set is then returned."""

    rivers = set()
    for station in stations:
        rivers.add(station.river)

    return rivers


def stations_by_river(stations):
    """This functions requires the list of stations to be passed in. It creates a list of rivers using the
    rivers_with_stations function. It then iterates through the list of rivers. For each river, it iterates through the
    list of stations to check if each station is on that river. If it is, it is added to a list. The river and list
    combination is added to the dictionary stations_on_river, with the river being the key. It then moves onto the next
    river in the list, and finally returns the completed dictionary."""

    stations_on_river = {}
    rivers = rivers_with_stations(stations)
    for river in rivers:
        riverstations = []

        for station in stations:
            if station.river == river:
                riverstations.append(station.name)
                riverstations.sort()

        stations_on_river[river] = riverstations

    return stations_on_river


def rivers_by_station_number(stations, N):
    """The function takes 2 arguments: the number stations list, and N, the number of rivers to be returned which have
    the most stations on them. The list is then sorted by number of stations. It uses the stations_by_river function to
    obtain a dictionary with each river being a key, and a list of stations on the river as the value. This function
    then uses that dictionary to create a list of tuples, each containing the name of the river, and the number of
    stations on it. A new list is created containing only the number of stations which then is used to check if the
    N+1th river has the same number of stations as the Nth. If it does, the number of N is increased and this check is
    then repeated. The first N (the new value of N) in the list of tuples is then returned"""
    
    stations_on_river = stations_by_river(stations)
    station_numbers = []
    for i, j in stations_on_river.items():
        station_numbers.append((i, len(j)))

    station_numbers.sort(key=lambda x: x[1], reverse=True)
    numbers = [b[1] for b in station_numbers]
    c = []
    d = 0
    e = 0
    n = N
    while d == e:
        d = numbers[n-1]
        e = numbers[n]
        if d == e:
            n += 1 

    for i in range(n):
        c.append(station_numbers[i])

    return c
