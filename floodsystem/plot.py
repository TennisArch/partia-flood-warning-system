import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.station import MonitoringStation
from floodsystem.analysis import polyfit

def plot_water_levels(station, dates, levels):

    higher_value = []
    lower_value = []
    for i in range(len(levels)):
        higher_value.append(station.typical_range[1])
        lower_value.append(station.typical_range[0])
    
    plt.plot(dates, levels, 'b', label = "water level")
    plt.plot(dates, lower_value, 'g', label = 'typical low')
    plt.plot(dates, higher_value, 'r', label = 'typical high')

    plt.xlabel('date')
    plt.ylabel('Water Level')
    plt.xticks(rotation = 45);
    plt.title(station.name)
    plt.legend()

    plt.tight_layout()

    return plt.show()



def plot_water_level_with_fit(station, dates, levels, p):

    '''plots the water level against the date, the typical high an low levels, and the polynomial fit for each station'''

    poly_float_dates, poly = polyfit(dates, levels, p)
    print(poly_float_dates)

    typical_high = []
    typical_low = []
    for i in range(len(levels)):
        typical_high.append(station.typical_range[1])
        typical_low.append(station.typical_range[0])

    plt.plot(dates, levels, 'b', label = 'water level')
    plt.plot(dates, poly(poly_float_dates), 'r', label = "poly fit")
    plt.plot(dates, typical_high, label = 'typical high water level')
    plt.plot(dates, typical_low, label = 'typical low water level')

    plt.xlabel('date')
    plt.ylabel('Water Level')
    plt.xticks(rotation=45);
    plt.title(station.name)
    plt.legend()

    plt.tight_layout()


    return plt.show()