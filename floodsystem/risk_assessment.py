from floodsystem.stationdata import update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import datetime

def assess_risk(stations, test = False):

    '''assesses the risk of flooding. The criteria is as follows:
    -If the relative water level is greater or equal to 3, flood risk is severe
    -If the relative water level is less than or equal to 1, flood risk is low
    -If relative water level is between 1 and 2, and the mean flood level has increased from day to day for the past
        two days, flood risk is high
    -If relative water level is between 1 and 2, and the mean flood level has decreased from day to day for the past
        two days, flood risk is low
    -If relative water level is between 1 and 2, and the previous two conditions have not been met, flood risk is
        moderate
    -If relative water level is between 2 and 3, and the mean flood level has increased from day to day for the past
        two days, flood risk is severe
    -If relative water level is between 2 and 3, and the mean flood level has decreased from day to day for the past
        two days, flood risk is moderate
    -If relative water level is between 1 and 2, and the previous two conditions have not been met, flood risk is
        high

    if the water level data is not available for the past 72 hours, then no flood risk classification is issued for that
    station. The function has a test mode, which enables the update water levels function, and the dates and levels
    fetcher function to be bypassed, and the artificial station data to be used instead
    '''

    low = []
    moderate = []
    high = []
    severe = []
    if test == False:
        update_water_levels(stations)

    for station in stations:
        if station.relative_water_level() is not None:
            if station.relative_water_level() <= 1:
                low.append(station)
                continue

            if station.relative_water_level() >= 3:
                severe.append(station)
                continue

            if 1 < station.relative_water_level() < 3:
                dt = 3
                if test == False:
                    dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
                else:
                    levels = gen_test_levels()

                if len(levels) <= 72:
                    print('flood warning not issued for the station ' + str(station.name))
                    continue

                day_1_mean = day_mean_relative_level(0, 23, levels)
                day_2_mean = day_mean_relative_level(24, 47, levels)
                day_3_mean = day_mean_relative_level(48, 71, levels)

                if station.relative_water_level() <= 2:

                    if day_1_mean > day_2_mean and day_2_mean > day_3_mean:
                        high.append(station)
                        continue

                    if day_3_mean > day_2_mean and day_2_mean > day_1_mean:
                        low.append(station)
                        continue
                    else:
                        moderate.append(station)

                elif station.relative_water_level() > 2:
                    if day_1_mean > day_2_mean and day_2_mean > day_3_mean:
                        severe.append(station)
                        continue
                    if day_3_mean > day_2_mean and day_2_mean > day_1_mean:
                        moderate.append(station)
                    else:
                        high.append(station)

    return low, moderate, high, severe


def day_mean_relative_level(start, end, levels):
    '''calculates the mean water level between a given start and end point in the list of levels. occasionally the data
    fetcher returns a particular level as a list instead of a float. If this is the case, the first item in the list
    is taken to be the level at that point.'''

    day_total = 0
    for i in range(start, end):
        if type(levels[i]) == list:
            day_total += levels[i][0]
            continue
        day_total += levels[i]

    day_mean = day_total / (end - start)
    return day_mean


def towns_at_risk(group):
    '''takes a list of stations, and returns a set of all the towns where those stations are located. Some stations
    have a list as their town as opposed to a string, where the list gives a small settlement, and then a larger
    nearby city. If this is the case, then the smaller settlement is used as the town in order to provide more
    accurate flood warnings'''

    risk_set = set()
    for station in group:
        if type(station.town) == list:
            risk_set.add(station.town[0])
            continue
        risk_set.add(station.town)

    return risk_set

def gen_test_levels():
    '''used to generate levels during testing when assess_risk is in test mode. It creates a list where the mean level
    increases each day.'''
    levels = []
    for n in range(0, 73):
        if 0 <= n <= 24:
            levels.append(3)
        if 24 < n <= 48:
            levels.append(2)
        if n > 48:
            levels.append(1)
    return levels