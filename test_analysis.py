from floodsystem.analysis import polyfit
import numpy as np

def test_polyfit():
    '''tests the polynomial generation. for each x value, y is generated, where y = x^2. it then fits a polynomial
    to y, and finally asserts that poly(x) is close to y within a margin of error using np.allclose. Note this does not
    test the float date conversion in analysis.py. This is bypassed with a test mode'''

    x = [-1, 0, 1, 2, 3, 4, 5]
    y = []
    for i in range(7):
        y.append(x[i]**2)

    dates, poly = polyfit(x, y, 3, test_mode=True)

    assert np.allclose(y, poly(x))




test_polyfit()