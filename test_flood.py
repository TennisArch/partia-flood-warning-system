from floodsystem.flood import stations_highest_rel_level, stations_level_over_threshold
from floodsystem.station import MonitoringStation

def test_stations_level_over_threshold():

    s_id = "test-s-id1"
    m_id = "test-m-id1"
    label = "some station1"
    coord = (-2.0, 4.0)
    trange = (1, 3.4445)
    river = "River1 X"
    town = "My Town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id2"
    m_id = "test-m-id2"
    label = "some station2"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River2 X"
    town = "My Town2"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id3"
    m_id = "test-m-id3"
    label = "some station3"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River3 X"
    town = "My Town3"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    test_stations = [s1, s2, s3]

    i = 1
    for station in test_stations:

        station.latest_level = i
        MonitoringStation.relative_water_level(station)
        i += 1

    
    assert stations_level_over_threshold(test_stations, 0.8) == [("some station3", 2), ("some station2", 1)]

def test_stations_highest_relative_level():

    s_id = "test-s-id1"
    m_id = "test-m-id1"
    label = "some station1"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River1 X"
    town = "My Town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id2"
    m_id = "test-m-id2"
    label = "some station2"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River2 X"
    town = "My Town2"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id3"
    m_id = "test-m-id3"
    label = "some station3"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River3 X"
    town = "My Town3"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id4"
    m_id = "test-m-id4"
    label = "some station4"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River4 X"
    town = "My Town4"
    s4 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id5"
    m_id = "test-m-id5"
    label = "some station5"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River5 X"
    town = "My Town5"
    s5 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id6"
    m_id = "test-m-id6"
    label = "some station6"
    coord = (-2.0, 4.0)
    trange = (1, 2)
    river = "River6 X"
    town = "My Town7"
    s6 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    test_list = [s1, s2, s3, s4, s5, s6]

    i = 1
    for station in test_list:

        station.latest_level = i
        MonitoringStation.relative_water_level(station)
        i += 1

    assert stations_highest_rel_level(test_list, 3) == [("some station6", 5), ("some station5", 4), ("some station4", 3)]

