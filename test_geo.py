"""tests for the geo module"""


from floodsystem import geo
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation


def test_stations_by_distance():
    """Tests that two distances are correct. Firstly checks that the distance between a station and its coordinates
    is zero, and then checks that a known distance is correctly calculated"""

    stations = build_station_list()
    for station in stations:
        if station.name == 'Cambridge Jesus Lock':
            test_station = station
            p = test_station.coord
            test_distance = geo.stations_by_distance(stations, p)[:1]
            assert test_distance[0][1] == 0

        if station.name == 'Penberth':
            test_station = station
            p = (52.2053, 0.1218)
            test_distance = geo.stations_by_distance(stations, p)[-1:]
            assert round(test_distance[0][1] - 467.53431870130544 , 10) == 0


def test_stations_within_radius():
    """point p is the north pole. This tests there are no stations within 10km of the north pole, and all the stations
    are within a distance approximately equal to the circumference of the earth. It then creates a test list of stations
    where the only station is very close to the north pole, and checks this station is within 10km of p, but not exactly
     where p is"""


    stations = build_station_list()
    p = (90.0000, 135.0000)
    assert len(geo.stations_within_radius(stations, p, 10)) == 0
    assert len(geo.stations_within_radius(stations, p, 40075)) == len(stations)

    s_id = "test-s-id1"
    m_id = "test-m-id1"
    label = "some station1"
    coord = (90.0000, 130.00000)
    trange = (-2.3, 3.4445)
    river = "River1 X"
    town = "My Town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    test_list = [s1]
    assert len(geo.stations_within_radius(test_list, p, 5)) == 1
    assert len(geo.stations_within_radius(test_list, p, 0)) == 0

def test_stations_by_river():
    """Tests that the dictionary contains a UK river as expected, doesn't contain a non UK river, and that one of the
    UK rivers has an expected station within the dictionary value for that river."""

    stations = build_station_list()
    dict = geo.stations_by_river(stations)
    assert dict.__contains__('River Nile') == False
    assert dict.__contains__('River Thames') == True
    assert dict.__contains__('River Test') == True
    assert dict['River Thames'].__contains__("Windsor Park")

def test_rivers_by_station_number():
    """Tests that the function correctly returns a list of length 10 when asked for the top nine stations, and that
    expected rivers are on that list"""

    stations = build_station_list(use_cache=False)
    test_list = geo.rivers_by_station_number(stations, 9)
    assert len(test_list) == 9
    assert test_list.__contains__(('River Thames', 55)) == True
    assert test_list.__contains__(('River Wey', 2)) == False

def test_rivers_with_stations():
    """performs simple checks that the set of rivers contains a river within the UK with stations, doesn't contain a
    river outside the uk and doesn't contain a blank entry"""

    stations = build_station_list()
    rivers = geo.rivers_with_stations(stations)
    assert rivers.__contains__("River Thames") is True
    assert rivers.__contains__("River Nile") is False
    assert rivers.__contains__("") is False