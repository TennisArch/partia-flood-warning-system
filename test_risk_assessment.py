from floodsystem import risk_assessment
from floodsystem.station import MonitoringStation

def test_assess_risk():
    '''tests the assess_risk function. 3 stations are created. The first one has a relative water level below 1, so
    should be catagorized as a low flood risk. The second station has a relative water level between 2 and 3, and the
    mean water level increases each day for the past 3 days (see gen_test_levels), so it should be catagorized as
    severe risk. The third station has relative water level above 3, so should be catagorized as severe risk'''

    s_id = "test-s-id1"
    m_id = "id_1"
    label = "low test"
    coord = (90.0000, 130.00000)
    trange = (0, 1)
    river = "River1"
    town = "low town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id2"
    m_id = "id_2"
    label = "severe test"
    coord = (90.0000, 130.00000)
    trange = (0, 1)
    river = "River2"
    town = "severe town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id3"
    m_id = "id_3"
    label = "severe test 2"
    coord = (90.0000, 130.00000)
    trange = (0, 1)
    river = "River3"
    town = "severe town 2"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)



    test_list = [s1, s2, s3]


    i = 0.1
    for station in test_list:

        station.latest_level = i
        MonitoringStation.relative_water_level(station)
        i += 2


    low, moderate, high, severe = risk_assessment.assess_risk(test_list, test = True)
    severe_risk_towns = risk_assessment.towns_at_risk(severe)
    low_risk_towns = risk_assessment.towns_at_risk(low)
    print(severe_risk_towns)
    assert severe_risk_towns == {'severe town', 'severe town 2'}
    assert low_risk_towns == {'low town'}


def test_gen_test_levels():
    '''tests that the test_levels list is correctly created'''
    test_levels = risk_assessment.gen_test_levels()
    assert test_levels[0] > test_levels[30]
    assert test_levels[30] > test_levels[70]


