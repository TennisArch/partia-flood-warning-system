# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistency():
    """Creates 3 stations 1 with a valid range, 1 without a valid range and 1 with a range of none
    then tests function with them"""
    
    s_id = "test-s-id1"
    m_id = "test-m-id1"
    label = "some station1"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River1 X"
    town = "My Town1"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id2"
    m_id = "test-m-id2"
    label = "some station2"
    coord = (-2.0, 4.0)
    trange = (4.3, 3.4445)
    river = "River2 X"
    town = "My Town2"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s_id = "test-s-id3"
    m_id = "test-m-id3"
    label = "some station3"
    coord = (-2.0, 4.0)
    trange = None
    river = "River3 X"
    town = "My Town3"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    test_stations = [s1, s2, s3]
    assert inconsistent_typical_range_stations(test_stations) == ["some station2", "some station3"]
